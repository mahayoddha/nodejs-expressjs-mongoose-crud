$('.delete').on('click', function(e) {
  e.preventDefault();

  var confirmation = confirm('Are you sure you want to delete this record?');

  if (confirmation === true) {
  	$.ajax({
  	  type: 'DELETE',
  	  url: $(this).attr('href')
  	}).success(function(data, status) {
  	  if (typeof data.redirect == 'string') {
  	    window.location.replace(window.location.protocol + "//" + window.location.host + data.redirect);
  	  }
  	});
  } else {
  	return false;
  }
});

$('.update').on('submit', function(e) {
  e.preventDefault();

  $.ajax({
  	type: 'PUT',
  	data: $(this).serialize(),
  	url: $(this).attr('action')
  }).success(function(data, status) {
  	if (typeof data.redirect == 'string') {
  	  window.location.replace(window.location.protocol + "//" + window.location.host + data.redirect);
  	}
  });
});