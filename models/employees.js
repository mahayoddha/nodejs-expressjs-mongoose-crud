var mongoose = require('mongoose');

// Build the connection string
var dbURI = 'mongodb://localhost/employees';

// Create the database connection
mongoose.connect(dbURI, function(error, response) {
  if(error) {
  	console.log('Error while connecting to MongoDB Database. ' + error);
  } else {
  	console.log('Connected to database.');
  }
});

var EmployeeSchema = new mongoose.Schema({
  first_name: {type: String, require: true},
  last_name: {type: String, require: true}
});

// EmployeeSchema.path('model').validate(function (v) {
//   return ((v != "") && (v != null));
// });

module.exports = mongoose.model('Employee', EmployeeSchema);