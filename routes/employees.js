var Employee = require('../models/employees.js');
  
// Index
exports.index = function(request, response) {
  console.log("GET - /employees");
  return Employee.find(function(error, employees) {
    if (!error) {
      response.render('employees/index', {employees: employees});
    } else {
      response.statusCode = 500;
  	  console.log('Internal error (%d): %s', response.statusCode, error.message);
  	  return response.send({error: 'Server error'});
    }
  });
};

// Show
exports.show = function(request, response) {
  console.log("GET - /employees/:id");
  return Employee.findById(request.params.id, function(error, employee) {
  	if(!employee) {
  	  response.statusCode = 404;
  	  return response.send({error: 'Employee not found'});
  	}

  	if(!error) {
  	  response.render('employees/show', {employee: employee});
  	} else {
  	  response.statusCode = 500;
  	  console.log('Internal error (%d): %s', response.statusCode, error.message);
  	  return response.send({error: 'Server error'});
  	}
  });
}

// New
exports.new = function(request, response) {
  console.log('GET - /employees/new');
  response.render('employees/new');
}

// Create
exports.create = function(request, response) {
  console.log('POST - /employees');

  var employee = new Employee({
  	first_name: request.body.first_name,
  	last_name: request.body.last_name
  });

  employee.save(function(error) {
  	if(error) {
  	  console.log('Error while saving employee: ' + error);
      response.send({ error: error });
      return;
    } else {
      console.log("Employee record created.");
      response.redirect('/employees');
    }
  });
}

// Edit
exports.edit = function(request, response) {
  console.log('GET - /employees/:id/edit');
  return Employee.findById(request.params.id, function(error, employee) {
    if(!employee) {
      response.statusCode = 404;
      return response.send({error: 'Employee not found'});
    }

    if(!error) {
      response.render('employees/edit', {employee: employee});
    } else {
      response.statusCode = 500;
      console.log('Internal error (%d): %s', response.statusCode, error.message);
      return response.send({error: 'Server error'});
    }
  });
}

// Update
exports.update = function(request, response) {
  console.log('PUT - /employees/:id');
  return Employee.findById(request.params.id, function(error, employee) {
    if(!employee) {
      response.statusCode = 404;
      return response.send({error: 'Employee not found'});
    } else {
      if(request.body.first_name != null) employee.first_name = request.body.first_name;
      if(request.body.last_name != null) employee.last_name = request.body.last_name;
      return employee.save(function(error) {
        if(!error) {
          console.log('Employee updated.');
          response.statusCode = 200;
          response.send({success: 'Employee updated.', redirect: '/employees/' + employee._id});
        } else {
          if(error.name == 'ValidationError') {
            response.statusCode = 400;
            response.send({ error: 'Validation error' });
          } else {
            response.statusCode = 500;
            response.send({ error: 'Server error' });
          }
          console.log('Internal error (%d): %s', response.statusCode, error.message);
          return response.send({error: 'Server error'});
        }
      });
    }
  });
}

// Destroy
exports.destroy = function(request, response) {
  console.log('DELETE - /employees/:id');

  return Employee.findById(request.params.id, function(error, employee) {
    if(!employee) {
      response.statusCode = 404;
      return response.send({error: 'Employee not found.'});
    } else {
      return employee.remove(function(error) {
        if(!error) {
          console.log('Employee deleted.');
          response.statusCode = 200;
          response.send({success: 'Employee deleted.', redirect: '/employees'});
        } else {
          response.statusCode = 500;
          console.log('Internal error (%d): %s', response.statusCode, error.message);
          return response.send({error: 'Server error'});
        }
      });
    }
  });
}